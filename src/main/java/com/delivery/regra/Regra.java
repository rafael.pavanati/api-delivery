package com.delivery.regra;

import com.delivery.repository.AbstractEntityImpl;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.UUID;

@Entity
@Table(name = "VW_REGRAS", schema = "DELIVERY_OWNER")
public class Regra extends AbstractEntityImpl<UUID> {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private UUID id;

    @Column(name = "MULTA")
    private BigDecimal multa;

    @Column(name = "DIAS_ATRASO")
    private Integer diasAtraso;

    @Column(name = "JUROS_DIA")
    private BigDecimal jurosDia;

    @Override
    public UUID getId() {
        return id;
    }

    public BigDecimal getMulta() {
        return multa;
    }

    public Integer getDiasAtraso() {
        return diasAtraso;
    }

    public BigDecimal getJurosDia() {
        return jurosDia;
    }

}
