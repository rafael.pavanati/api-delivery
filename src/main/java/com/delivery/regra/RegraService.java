package com.delivery.regra;

import com.delivery.repository.BasicRepository;
import com.delivery.repository.Page;
import com.delivery.repository.Pageable;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.Optional;
import java.util.UUID;

@Service
public class RegraService {

    private final BasicRepository basicRepository;

    public RegraService(BasicRepository basicRepository) {
        this.basicRepository = basicRepository;
    }

    public Page<Regra> findAll(int offset, int limit) {
        return basicRepository.findAll(Regra.class, Pageable.of(offset, limit));
    }

    public Regra findById(UUID id) {
        return basicRepository.find(Regra.class, id);
    }

    public Regra create(Regra regra) {
        return basicRepository.save(regra);
    }

    public BigDecimal calculaJuros(long diasEmAtraso, BigDecimal valor) {
        Optional<Regra> optionalRegra = basicRepository.query(Regra.class).where(
                QRegra.regra.diasAtraso.lt(diasEmAtraso)
        ).fetch().stream().max(Comparator.comparing(Regra::getDiasAtraso));

        if (optionalRegra.isPresent()) {
            Regra regra = optionalRegra.get();
            double value = valor.doubleValue();
            value = value + ((value / 100) * regra.getMulta().doubleValue());
            value = value + ((value / 100) * (diasEmAtraso * regra.getJurosDia().doubleValue()));
            return BigDecimal.valueOf(value);
        }
        return valor;
    }


    public long delete(UUID id) {
        return basicRepository.delete(Regra.class, jpaDeleteClause -> jpaDeleteClause.where(QRegra.regra.id.eq(id))
        );
    }
}
