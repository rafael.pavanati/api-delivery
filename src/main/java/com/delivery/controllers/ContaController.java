package com.delivery.controllers;

import com.delivery.conta.Conta;
import com.delivery.conta.ContaService;
import com.delivery.repository.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.UUID;

@Controller
@RequestMapping("/api/conta")
public class ContaController {

    private final ContaService service;

    public ContaController(ContaService service) {
        this.service = service;
    }

    @GetMapping
    public ResponseEntity<Page<Conta>> findAll(@RequestParam(value = "offset", required = false, defaultValue = "0") int offset,
                                               @RequestParam(value = "limit", required = false, defaultValue = "20") int limit) {
        return ResponseEntity.ok(service.findAll(offset, limit));
    }

    @GetMapping(value = "{id}")
    public ResponseEntity<Conta> find(@PathVariable("id") final UUID id) {
        return ResponseEntity.ok(service.findById(id));
    }

    @DeleteMapping(value = "{id}")
    public ResponseEntity<Long> delete(@PathVariable("id") final UUID id) {
        return ResponseEntity.ok(service.delete(id));
    }

    @GetMapping(value = "juros/{id}")
    public ResponseEntity<BigDecimal> findJuros(@PathVariable("id") final UUID id) {
        return ResponseEntity.ok(service.calculaJuros(id));
    }

    @PostMapping
    public ResponseEntity<Conta> create(@RequestBody Conta conta) {
        return ResponseEntity.ok(service.create(conta));
    }
}
