package com.delivery.controllers;

import com.delivery.regra.Regra;
import com.delivery.regra.RegraService;
import com.delivery.repository.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@Controller
@RequestMapping("/api/regra")
public class RegraController {

    private final RegraService service;

    public RegraController(RegraService service) {
        this.service = service;
    }

    @GetMapping
    public ResponseEntity<Page<Regra>> findAll(@RequestParam(value = "offset", required = false, defaultValue = "0") int offset,
                                               @RequestParam(value = "limit", required = false, defaultValue = "20") int limit) {
        return ResponseEntity.ok(service.findAll(offset, limit));
    }

    @GetMapping(value = "{id}")
    public ResponseEntity<Regra> find(@PathVariable("id") final UUID id) {
        return ResponseEntity.ok(service.findById(id));
    }

    @DeleteMapping(value = "{id}")
    public ResponseEntity<Long> delete(@PathVariable("id") final UUID id) {
        return ResponseEntity.ok(service.delete(id));
    }

    @PostMapping
    public ResponseEntity<Regra> create(@RequestBody Regra regra) {
        return ResponseEntity.ok(service.create(regra));
    }
}
