package com.delivery.repository;

import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.PathBuilder;
import com.querydsl.core.types.dsl.PathBuilderFactory;
import com.querydsl.jpa.impl.JPADeleteClause;
import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import java.io.Serializable;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;

@Service
@Transactional
public class BasicRepository {

    @PersistenceContext
    EntityManager em;

    JPAQueryFactory jpaQueryFactory;

    @Autowired
    PathBuilderFactory pathBuilderFactory;

    @PostConstruct
    public void init() {
        jpaQueryFactory = new JPAQueryFactory(this.em);
    }


    public <T> T find(Class<T> entityClass, Serializable id) {
        return em.find(entityClass, id);
    }


    public <T> Page<T> findAll(Class<T> entityClass, Pageable pageable, Predicate... where) {
        JPAQuery<T> query = (JPAQuery) this.query(entityClass).where(where);
        long total = query.fetchCount();
        int offset = pageable.getOffset();
        int limit = pageable.getLimit();
        if ((long) offset > total) {
            offset = (int) (total / (long) limit) * limit;
        } else if ((long) offset == total) {
            offset = Math.max(offset - limit, 0);
        }

        query.offset((long) offset);
        query.limit((long) limit);
        this.applySorting(query, pageable.getSort());
        List<T> content = query.fetch();
        return new Page(content, pageable, total);
    }

    public <T> T findOne(Class<T> entityClass, Predicate... where) {
        return this.findOne(entityClass, (Sort) null, where);
    }


    public <T> T findOne(Class<T> entityClass, Sort sort, Predicate... where) {
        return (T) ((JPAQuery) this.applySorting((JPAQuery) this.query(entityClass).where(where), sort).limit(1L)).fetchOne();
    }

    public <T> JPAQuery<T> applySorting(JPAQuery<T> query, Sort sort) {
        Optional.ofNullable(sort)
                .ifPresent(s -> s.forEach(query::orderBy));
        return query;
    }

    public <T extends AbstractEntityImpl> T save(T entity) {
        if (!entity.isNew()) {
            this.em.merge(entity);
        } else {
            this.em.persist(entity);
        }
        this.em.flush();
        return entity;
    }


    public <T> JPAQuery<T> query(Class<T> entityClass) {
        JPAQuery<T> query = new JPAQuery(this.em);
        query.from(this.pathBuilderFactory.create(entityClass));
        return query;
    }

    public <T> long delete(Class<T> entityClass, Consumer<JPADeleteClause> consumer) {
        PathBuilder<T> entityPath = this.pathBuilderFactory.create(entityClass);
        JPADeleteClause clause = new JPADeleteClause(this.em, entityPath);
        consumer.accept(clause);

        try {
            return clause.execute();
        } catch (PersistenceException var6) {
            return -1L;
        }
    }

    public <T> long delete(Class<T> entityClass, Predicate... predicates) {
        return this.delete(entityClass, (query) -> {
            query.where(predicates);
        });
    }

    public <T> boolean exists(Class<T> entityClass, Predicate... predicates) {
        return this.query(entityClass).where(predicates).fetchCount() > 0;
    }
}
