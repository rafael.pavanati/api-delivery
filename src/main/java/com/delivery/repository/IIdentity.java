package com.delivery.repository;

public interface IIdentity<T> {
    T getId();
}
