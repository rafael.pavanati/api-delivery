package com.delivery.repository;

import java.io.Serializable;

public interface IEntity<I> extends Serializable, IIdentity<I> {
    default boolean isNew() {
        return this.getId() == null;
    }
}
