package com.delivery.repository;

import java.util.Objects;

public abstract class EntityBuilder<T> {

    protected T entity;

    protected void afterValidate() {
    }

    public final T build() {
        Objects.requireNonNull(this.entity);
        //TODO:criar validador no builder

        this.afterValidate();

        T var1;
        try {
            var1 = this.entity;
        } finally {
            this.entity = null;
        }

        return var1;
    }

}
