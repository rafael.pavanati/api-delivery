package com.delivery.conta;

import com.delivery.repository.AbstractEntityImpl;
import com.delivery.repository.EntityBuilder;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Objects;
import java.util.UUID;

import static java.time.temporal.ChronoUnit.DAYS;

@Entity
@Table(name = "VW_CONTAS", schema = "DELIVERY_OWNER")
public class Conta extends AbstractEntityImpl<UUID> {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private UUID id;

    @NotNull(message = "A conta deve ter um nome")
    @Column(name = "NOME")
    private String nome;

    @NotNull(message = "A conta deve ter uma data de vencimento")
    @DateTimeFormat(pattern = "dd-MM-yyyy")
    @Column(name = "DATA_VENCIMENTO")
    private LocalDate dataVencimento;

    @Column(name = "DATA_PAGAMENTO")
    @DateTimeFormat(pattern = "dd-MM-yyyy")
    private LocalDate dataPagamento;

    @Column(name = "VL_ORIGINAL")
    private BigDecimal valor;

    @Column(name = "VL_CORRIGIDO")
    private BigDecimal valorCorrigido;

    @Override
    public UUID getId() {
        return id;
    }

    @JsonProperty("diasEmAtraso")
    public long getDiasEmAtraso() {
        LocalDate dataRef = Objects.nonNull(this.dataPagamento) ? this.dataPagamento : LocalDate.now();
        return DAYS.between(this.dataVencimento, dataRef);
    }

    public String getNome() {
        return nome;
    }

    public LocalDate getDataVencimento() {
        return dataVencimento;
    }

    public LocalDate getDataPagamento() {
        return dataPagamento;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public BigDecimal getValorCorrigido() {
        return valorCorrigido;
    }


    public static class Builder extends EntityBuilder<Conta> {


        protected Builder(Conta entity) {
            this.entity = entity;
        }

        public static Builder create(Conta notaFiscal) {
            return new Builder(new Conta());
        }

        public static Builder from(Conta entity) {
            return new Builder(entity);
        }

        public Builder nome(String nome) {
            entity.nome = nome;
            return this;
        }

        public Builder dataVencimento(LocalDate dataVencimento) {
            entity.dataVencimento = dataVencimento;
            return this;
        }

        public Builder dataPagamento(LocalDate dataPagamento) {
            entity.dataPagamento = dataPagamento;
            return this;
        }

        public Builder valor(BigDecimal valor) {
            entity.valor = valor;
            return this;
        }

        public Builder valorCorrigido(BigDecimal valorCorrigido) {
            entity.valorCorrigido = valorCorrigido;
            return this;
        }

    }
}
