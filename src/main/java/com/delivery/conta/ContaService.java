package com.delivery.conta;

import com.delivery.regra.RegraService;
import com.delivery.repository.BasicRepository;
import com.delivery.repository.Page;
import com.delivery.repository.Pageable;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Objects;
import java.util.UUID;

@Service
public class ContaService {

    private final BasicRepository basicRepository;
    private final RegraService regraService;

    public ContaService(BasicRepository basicRepository, RegraService regraService) {
        this.basicRepository = basicRepository;
        this.regraService = regraService;
    }

    public Page<Conta> findAll(int offset, int limit) {
        Page<Conta> page = basicRepository.findAll(Conta.class, Pageable.of(offset, limit));
        page.getContent().forEach(conta -> {
            if (Objects.isNull(conta.getDataPagamento())) {
                Conta.Builder.from(conta).valorCorrigido(calculaJuros(conta)).build();
            }
        });
        return page;
    }

    public Conta findById(UUID id) {
        return basicRepository.find(Conta.class, id);
    }

    public Conta create(Conta conta) {
        Conta.Builder builder = Conta.Builder.from(conta);
        if (Objects.nonNull(conta.getDataPagamento())) {
            builder.valorCorrigido(calculaJuros(conta));
        }
        return basicRepository.save(builder.build());
    }

    public BigDecimal calculaJuros(UUID id) {
        return calculaJuros(findById(id));
    }

    public BigDecimal calculaJuros(Conta conta) {
        long diasEmAtraso = conta.getDiasEmAtraso();
        if (diasEmAtraso > 0) {
            return regraService.calculaJuros(diasEmAtraso, conta.getValor());
        } else {
            return new BigDecimal(0);
        }
    }

    public long delete(UUID id) {
        return basicRepository.delete(Conta.class, jpaDeleteClause -> jpaDeleteClause.where(QConta.conta.id.eq(id))
        );
    }
}
