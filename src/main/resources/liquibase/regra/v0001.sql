--liquibase formatted sql

-- changeset rafael.pavanati:1
CREATE TABLE IF NOT EXISTS DELIVERY_OWNER.REGRAS(
        ID uuid NOT NULL DEFAULT uuid_generate_v1 (),
        MULTA            NUMERIC(19,4) NOT NULL,
        JUROS_DIA        NUMERIC(19,4) NOT NULL,
        DIAS_ATRASO      INTEGER       NOT NULL,
        AUD_DH_ALTERACAO TIMESTAMP (6),
        PRIMARY KEY (ID)
    );
-- roolback DROP VIEW IF EXISTS DELIVERY_OWNER.VW_REGRAS;
-- roolback DROP TABLE IF EXISTS DELIVERY_OWNER.REGRAS;

-- changeset rafael.pavanati:2
CREATE OR REPLACE VIEW DELIVERY_OWNER.VW_REGRAS AS SELECT * FROM DELIVERY_OWNER.REGRAS;
-- roolback not required

