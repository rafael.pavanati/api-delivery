--liquibase formatted sql

-- changeset rafael.pavanati:1
CREATE TABLE IF NOT EXISTS DELIVERY_OWNER.CONTAS(
        ID uuid NOT NULL DEFAULT uuid_generate_v1 (),
        NOME             VARCHAR (500) NOT NULL,
        VL_ORIGINAL      NUMERIC(19,4),
        VL_CORRIGIDO     NUMERIC(19,4),
        DATA_VENCIMENTO  TIMESTAMP (6),
        DATA_PAGAMENTO   TIMESTAMP (6),
        AUD_DH_ALTERACAO TIMESTAMP (6),
        PRIMARY KEY (ID)
    );
-- roolback DROP VIEW IF EXISTS DELIVERY_OWNER.VW_CONTAS;
-- roolback DROP TABLE IF EXISTS DELIVERY_OWNER.CONTAS;

-- changeset rafael.pavanati:2
CREATE OR REPLACE VIEW DELIVERY_OWNER.VW_CONTAS AS SELECT * FROM DELIVERY_OWNER.CONTAS;
-- roolback not required

