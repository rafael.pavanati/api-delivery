FROM openjdk:8u111-jdk-alpine
VOLUME /tmp
ADD /target/*.jar app.jar
RUN apk --update add fontconfig ttf-dejavu
RUN apk add ttf-dejavu
ENTRYPOINT ["java", "-Djava.security.egd=file:/dev/./urandom", "-Djava.awt.headless=true", "-Dspring.profiles.active=prod","-jar","app.jar"]